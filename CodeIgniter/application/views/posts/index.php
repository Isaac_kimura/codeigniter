<h2><b><?= $title ?></b></h2>
<?php foreach($posts as $post) : ?>
	<div class="row">
		<div class="col-md-3">
			<img class = "post-thumb" src="<?php echo site_url();?>assets/images/posts/<?php echo $post['post_image']; ?> ">

	 
		</div>
		<div>
		<h4><?php echo $post['title']; ?></h4>
		<div class="container">	
			  <a href="#"><i class="fa fa-thumbs-o-up" style="font-size:24px"></i><span class="badge">5</span></a><br>
			  <a href="#"><i class="fa fa-comments" style="font-size:24px"></i> <span class="badge">10</span></a><br>
			  <a href="#">Followers <span class="badge">2</span></a>
		</div>
		</div>
		<div class="col-md-9">
			<small class="post-date">Posted On: <?php echo $post['created_at']; ?> in <strong><?php echo $post['name']; ?></strong></small><br>
			<div class="well">
			<?php echo word_limiter($post['body'], 60)?>
			</div>			
			<br> <br>
			<p><a class = "btn btn-primary" href="<?php echo site_url('/posts/'.$post['slug']); ?>">Read More</a></p>
		</div>
	</div>
	
<?php endforeach; ?>

<div class="paginate-link">
	<?php echo $this->pagination->create_links(); ?>
</div>